var productService = require('../services/products.js');

/**
 * Default page
 */
exports.index = function(req, res) {
  res.render('index', {
    title: 'Products',
    products: productService.getProducts(req.query.q),
  });
};

/**
 * Individual product page with more details
 */
exports.product = function(req, res) {
  var product = productService.getProduct(req.params.productId);

  if (product) {
    res.render('product', {
      title: product.Title.TitleText,
      product: product,
    });
  } else {
    res.status(404).render('404', {
      title: '"' + req.params.productId + '" not found',
    });
  }
};
