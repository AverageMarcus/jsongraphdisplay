# JSON Graph Display

## Required Tools

* NodeJS & NPM - https://nodejs.org/en/
* Grunt - `npm install -g grunt-cli`

## Libraries / Frameworks Used

* [Express](http://expressjs.com/en/index.html)
* [Handlebars](http://handlebarsjs.com/)

## How To Run

```
npm install
grunt
node app.js
```

### Using Docker

* `docker build -t json-graph-display .`
* `docker run -d -p 3000:3000 json-graph-display`
* Navigate to [http://localhost:3000](http://localhost:3000)

## Deploying

### Prerequisites

* [Create new EC2 instance with Linux](https://aws.amazon.com/ec2/)
* Install Git - `sudo yum install git` or `sudo apt-get update && sudo apt-get install git`
* [Install Docker](https://docs.docker.com/engine/installation/) - `wget -qO- https://get.docker.com/ | sh`
* `git clone https://AverageMarcus@bitbucket.org/AverageMarcus/jsongraphdisplay.git ~/src/jsongraphdisplay`

### Running

* Start Docker service - E.g. `sudo service docker start`
* `cd ~/src/jsongraphdisplay`
* `git pull`
* `docker build -t json-graph-display .`
* `docker run -d -p 80:3000 --restart=on-failure json-graph-display`

### Running Production

* Start Docker service - E.g. `sudo service docker start`
* `cd ~/src/jsongraphdisplay`
* `git pull`
* `docker build -t json-graph-display .`
* `docker run -d -p 80:8080 --restart=on-failure -e NODE_ENV='production' json-graph-display`

### Updating a running instance

* `cd ~/src/jsongraphdisplay`
* `git pull`
* `docker build -t json-graph-display .`
* `docker stop {RUNNING_CONTAINER_ID} && docker run -d -p 80:8080 --restart=on-failure -e NODE_ENV='production' json-graph-display`


## Project Structure

* `app.js` - Main entrypoint file. Contains Express.js setup and config.
* `config.js` - Contains site-wide config split into multiple environments (e.g. dev and prod)
* `controllers` - Map the data to a view and return the rendered page
* `public` - Contains all client-side assets such as CSS and JavaScript
* `routes` - Contains the mappings between URLs and Controllers
* `services` - Contains files that interact with external services such as datastores or webservices
* `views` - Contains Handlebars templates. `_partials` contains reusable snippets
* `.jscsrc` - See http://jscs.info/
* `.editorconfig` - See http://editorconfig.org/

## StyleSheets

All CSS stylesheets are generated from `.less` files in the `public/less` directory by Grunt. `.css` files are ignored from Git.

## Grunt

Grunt is used to performs several tasks before running the applications. These include:

* Running [JSCS](http://jscs.info/) against all JavaScript files to check for code style consistency
* Compiling Less files to CSS
* Adding all needed CSS prefixes
* Concatenating all CSS into a single file
* Minifying CSS and JavaScript

## Adding a New Page

To add a new page you will first need to add the appropriate [Handlebars](http://handlebarsjs.com/) template into the `views` directory. 
Once this is done you can create a new controller function that calls `res.render` with the new page.
Finally you will need to add a new route to `routes/index.js` such as:
```
indexRouter.route('/myNewPage').all(controllers.myNewPageFunction);
```

The `.all` means it will use that function for any of the HTTP verbs. If, for example, you just wanted it to respond on GET requests you would change that to `.get` instead.
