FROM centos:centos6

RUN rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
RUN yum install -y -q npm

ADD . /src

RUN npm install -g grunt-cli
RUN cd /src; npm install; grunt build

CMD ["node", "/src/app.js"]
