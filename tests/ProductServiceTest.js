var mocha = require('mocha');
var should = require('chai').should();

var productService = require('../services/products.js');

describe('ProductService', function() {
  describe('getProducts', function() {
    it('should return 25 products with no filter', function() {
      var products = productService.getProducts();
      should.exist(products);
      products.length.should.equal(25);
    });

    it('should return 25 products with empty filter', function() {
      var products = productService.getProducts('');
      should.exist(products);
      products.length.should.equal(25);
    });

    it('should return 0 products with non-matching filter', function() {
      var products = productService.getProducts('not matching anything');
      should.exist(products);
      products.length.should.equal(0);
    });

    it('should return 3 products with filter "test"', function() {
      var products = productService.getProducts('test');
      should.exist(products);
      products.length.should.equal(3);
    });
  });

  describe('getProduct', function() {
    it('should return a product for ID "9780000001306"', function() {
      var product = productService.getProduct('9780000001306');
      should.exist(product);
      product.Title.TitleText.should.equal('CAM Testing: BJ Migration patch 1 2011...');
    });

    it('should return "undefined" for ID "123"', function() {
      var product = productService.getProduct('123');
      should.not.exist(product);
    });
  });
});
