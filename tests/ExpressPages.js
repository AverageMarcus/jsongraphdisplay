var request = require('supertest');

describe('Pages', function() {
  var server;

  beforeEach(function() {
    server = require('../app');
  });

  it('should respond to "/"', function(done) {
    request(server)
      .get('/')
      .expect(200, done);
  });

  it('should respond to searches', function(done) {
    request(server)
      .get('/?q=test')
      .expect(200, done);
  });

  it('should respond to "/product/9780000001306"', function(done) {
    request(server)
      .get('/product/9780000001306')
      .expect(200, done);
  });

  it('should respond with 404 to "/product/123"', function(done) {
    request(server)
      .get('/product/123')
      .expect(404, done);
  });

  it('should respond with 404 to "/product"', function(done) {
    request(server)
      .get('/product')
      .expect(404, done);
  });

  it('should respond with "404" for a page that doesn\'t exist', function(done) {
    request(server)
      .get('/none')
      .expect(404, done);
  });
});
