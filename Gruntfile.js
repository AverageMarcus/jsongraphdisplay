module.exports = function(grunt) {
  var config = {
    jscs: {
      options: {
        config: '.jscsrc',
        fix: true, // attempt to auto-fix mistakes
        esnext: false,
        excludeFiles: ['public/js/app.min.js'],
      },
      gruntfile: 'Gruntfile.js',
      server: ['controllers/**/*.js', 'models/**/*.js', 'routes/**/*.js', 'services/**/*.js', 'app.js'],
      client: 'public/js/**/*.js',
    },
    concat: {
      css: {
        // add your css files over here to concatenate all css files
        // let's save our site users some bandwith
        files: {
          'public/css/app.styles.min.css': ['public/css/cssreset.min.css', 'public/css/styles.min.css'],
        },
      },
    },
    uglify: {
      options: {
        mangle: {
          except: ['jQuery'],
        },
      },
      dist: {
        files: {
          // add your js files over here to minify them into one javascript source file
          'public/js/app.min.js': ['public/js/main.js'],
        },
      },
    },
    less: {
      src: {
        files: [{
          expand: true,
          cwd: 'public/less',
          src: ['**/*.less', '!**/_*.less'],
          dest: 'public/css',
          ext: '.css',
        },],
      },
    },
    cssmin: {
      src: {
        files: [{
          expand: true,
          cwd: 'public/css',
          src: '**/*.css',
          dest: 'public/css',
          ext: '.min.css',
        },],
      },
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', '> 1%'],
      },
      main: {
        expand: true,
        flatten: true,
        src: 'public/css/*.css',
        dest: 'public/css',
      },
    },
    clean: {
      css: ['public/css/*.css', '!public/css/app.styles.min.css'],
    },
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
        },
        src: ['tests/**/*.js'],
      },
    },
    watch: {
      all: {
        files: ['public/**/*', 'views/**', 'tests/*', '!**/node_modules/**', '!public/vendor/**/*', '!**/*.min.*'],
      },
      scripts: {
        files: ['public/js/**/*.js', '!**/*.min.*'],
        tasks: ['jscs:client', 'uglify:dist', 'mochaTest'],
      },
      less: {
        files: ['public/less/**/*.less'],
        tasks: ['less', 'cssmin', 'concat:css', 'clean:css'],
      },
      test: {
        files: ['tests/*.js', 'services/*.js'],
        tasks: ['mochaTest'],
      },
    },
  };

  grunt.initConfig(config);

  // Load the tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.registerTask('build', ['jscs', 'uglify:dist', 'less', 'autoprefixer', 'cssmin', 'concat:css', 'clean:css', 'mochaTest']);
  grunt.registerTask('default', ['build', 'watch']);
};
