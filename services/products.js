var config = require('../config');
var data = require(config.datasource.url);

/**
 * Retrieves all products from the datastore sorted by TitleText
 *
 * @param  {string} query (Optional) filter products to those that contain this text within one of their properties
 * @return {array} an array of all products, optionally filtered by a provided query
 */
exports.getProducts = function(query) {
  var productTitles = [];
  query = (query || '').toLowerCase();

  /**
   * Iterates over all properties for string values and checks if they contain the provided query.
   * This does a case-insensitive search.
   *
   * @param  {object} product The individual product to check for matching values
   * @return {boolean} Returns true if any match was found
   */
  var matchesFilter = function(product) {
    for (var key in product) {
      if (product.hasOwnProperty(key) && typeof product[key] === 'string') {
        if (product[key].toLowerCase().indexOf(query) >= 0) {
          return true;
        }
      } else if (product.hasOwnProperty(key) && typeof product[key] === 'object') {
        if (matchesFilter(product[key])) {
          return true;
        }
      }
    }

    return false;
  };

  for (var product in data.worksById) {
    if (query === undefined || query.length === 0 || matchesFilter(data.worksById[product])) {
      var productToAdd = data.worksById[product];
      productToAdd.ProductId = product; // Include product ID within object
      productTitles.push(productToAdd);
    }
  }

  // Sort the list by TitleText ascending
  productTitles.sort(function(a, b) {
    if (a.Title.TitleText.toLowerCase() === b.Title.TitleText.toLowerCase()) return 0;
    if (a.Title.TitleText.toLowerCase() > b.Title.TitleText.toLowerCase()) return 1;
    if (a.Title.TitleText.toLowerCase() < b.Title.TitleText.toLowerCase()) return -1;
  });

  return productTitles;
};

/**
 * Gets a single product that matches the provided ID
 *
 * @param  {string} productId The ID of the product to return
 * @return {object} The individual product if found, undefined if not.
 */
exports.getProduct = function(productId) {
  var product = data.worksById[productId];
  if (product) {
    product.ProductId = productId;
  }

  return product;
};
