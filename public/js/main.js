window.onload = function() {
  var productSearch = document.getElementById('productSearch');
  var sortProducts = document.getElementById('sortProducts');

  if (productSearch) {
    productSearch.onkeydown = function(event) {
      var keyPressed = event.keyCode || event.which;
      if (keyPressed == 13) {
        window.location = '/?q=' + this.value;
      }
    };

    // Auto-focus on the search box as the user starts typing
    document.onkeydown = function(event) {
      if (document.activeElement !== productSearch) {
        var keyPressed = String.fromCharCode(event.keyCode || event.which);
        if (keyPressed.match(/[a-zA-Z]/)) {
          productSearch.value = '';
          productSearch.focus();
        }
      }
    };
  }

  if (sortProducts) {
    sortProducts.onclick = function(event) {
      var productsList = document.querySelector('.products');
      if (productsList.className.indexOf('reverse') >= 0) {
        productsList.className = productsList.className.replace(/reverse/, '');
      } else {
        productsList.className += ' reverse';
      }

      event.preventDefault();
    };
  }
};
