/**
 * Module dependencies.
 */
var express = require('express');
var path = require('path');
var hbs = require('express-hbs');
var logger = require('morgan');
var bodyParser = require('body-parser');
var compress = require('compression');
var favicon = require('static-favicon');
var methodOverride = require('method-override');
var errorHandler = require('errorhandler');
var config = require('./config');
var routes = require('./routes');

var app = express();

/**
 * Express configuration.
 */
app.set('port', config.server.port);
app.engine('hbs', hbs.express3({
  partialsDir: __dirname + '/views/partials',
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

hbs.registerHelper({
  eq: function(v1, v2) {
    return v1 === v2;
  },

  ne: function(v1, v2) {
    return v1 !== v2;
  },

  lt: function(v1, v2) {
    return v1 < v2;
  },

  gt: function(v1, v2) {
    return v1 > v2;
  },

  lte: function(v1, v2) {
    return v1 <= v2;
  },

  gte: function(v1, v2) {
    return v1 >= v2;
  },

  and: function(v1, v2) {
    return v1 && v2;
  },

  or: function(v1, v2) {
    return v1 || v2;
  },

  isString: function(a) {
    return (typeof a === 'string');
  },
});

app
  .use(compress())
  .use(favicon())
  .use(logger('dev'))
  .use(bodyParser())
  .use(methodOverride())
  .use(express.static(path.join(__dirname, 'public')))
  .use(routes.indexRouter)
  .use(function(req, res) {
    res.status(404).render('404', {title: 'Not Found :('});
  });

if (app.get('env') === 'development') {
  app.use(errorHandler());
}

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = server;
