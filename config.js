var config = {
  development: {
    server: {
      port: 3000,
    },
    datasource: {
      url: '../data/graph.json',
    },
  },
  testing: {
    server: {
      port: 3001,
    },
    datasource: {
      url: '/data/jsongraph.test.json',
    },
  },
  production: {
    server: {
      port: 8080,
    },
    datasource: {
      url: '/data/jsongraph.json',
    },
  },
};

module.exports = config[process.env.NODE_ENV] || config.development;
